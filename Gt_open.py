#!/usr/bin/python3
# -*- coding: utf-8 -*-

try:
    import pandas
    import biopandas
    import gi
    gi.require_version('Gtk', '3.0')
    from gi.repository import Gtk

except ImportError:
    # Que hacer si el módulo no se puede importar
    print("""
    Módulo no instalado. Favor, instale las librerías necesarias para
    la ejecución del código, las cuales son:
    > Pandas
    > Biopandas
    > Gi
    > pyMOL
    """)


class MainWindows():
    def __init__(self):
        #Ventana
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./AppProtein.glade")

        self.window = self.builder.get_object("main_windows")
        self.window.set_title("Protein")
        self.window.connect("destroy", Gtk.main_quit)
        self.window.set_default_size(800, 600)
        self.window.show_all()

        #Instanciar botones
        self.btn_abrir_filechooser = self.builder.get_object("abrir_filechooser")
        self.btn_abrir_filechooser.connect("clicked", self.apertura_filechooser)

        self.btn_abrir_about= self.builder.get_object("abrir_about")
        self.btn_abrir_about.connect("clicked", self.apertura_about)

        #Combobox y Liststore
        self.combobox = self.builder.get_object("combobox_main")
        self.caracteristicas = Gtk.ListStore(str)

        cell = Gtk.CellRendererText()

        self.combobox.pack_start(cell, True)
        self.combobox.add_attribute(cell,"text", 0)
        caracteristicas = ["ATOM",
                           "HETATM",
                           "ANISOU",
                           "OTHERS"]

        for caracteristica in caracteristicas:
            self.caracteristicas.append([caracteristica])

        self.combobox.set_model(model=self.caracteristicas)
        self.combobox.connect("changed", self.seleccion)

        #self.resumen_proteina = self.get_object ("resumen_proteina")
        #self.imagen_proteina
        #self.dataframe

    def seleccion(self, btn=None):
        indice = self.combobox.get_active_iter()
        modelo = self.combobox.get_model()
        opcion = modelo[indice][0]

        if opcion == "ATOM":
            print("Ingreso a ATOM")
            #self.resumen_proteina
            #self.imagen_proteina.set_from_file("./imagen_proteina.png")

        elif opcion == "HETATM":
            print("Ingreso a HETATM")
            # self.resumen_proteina
            # self.imagen_proteina.set_from_file("./imagen_proteina.png")

        elif opcion == "ANISOU":
            print("Ingreso a ATOM")
            # self.resumen_proteina
            # self.imagen_proteina.set_from_file("./imagen_proteina.png")

        elif opcion == "OTHERS":
            print("Ingreso a OTHERS")
            # self.resumen_proteina
            # self.imagen_proteina.set_from_file("./imagen_proteina.png")

        # Label informativo
    
    #Boton "Abrir" para que abra filechooser
    def apertura_filechooser(self,btn=None):  
        filechooser = FileChooser()
    
    #Boton "Acerca de" para que abra la ventana about_dialog
    def apertura_about(self,btn=None):
        about = About_Dialog()


# Clase para FILECHOOSER
class FileChooser():

    # Se define función de constructor
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./AppProtein.glade")
        self.chooser_win = self.builder.get_object("file_chooser")
        self.chooser_win.set_default_size(1000, 800)
        self.chooser_win.show_all()

        # Filtrado de archivos~carpetas, que solo permitirá la
        # selección de archivos en formato .pdb
        pdb_filter = Gtk.FileFilter()
        pdb_filter.set_name('Valid Format (.pdb)')
        pdb_filter.add_pattern('*.pdb')
        self.chooser_win.add_filter(pdb_filter)

        self.accept_btn = self.builder.get_object('accept_btn')
        self.cancel_btn = self.builder.get_object('cancel_btn')
        #! self.cancel_btn.connect('clicked', self.cancel_pressed)

class About_Dialog():
    # Constructor
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("AppProtein.glade")

        self.emergente = self.builder.get_object("about_dialog")
        self.emergente.show_all()

        self.btn_aceptar = self.builder.get_object("aceptar")
        self.btn_aceptar.connect("clicked", self.ok_press)

    # Cerrar
    def ok_press(self, btn=None):
        self.emergente.destroy()


if __name__ == "__main__":
    MainWindows()
    Gtk.main()

